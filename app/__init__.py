
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

migrate = Migrate(app, db)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'auth.login'

from app import views, models, auth

from .views import views_bp
from .auth import auth_bp

app.register_blueprint(views_bp, url_prefix = '/')
app.register_blueprint(auth_bp, url_prefix = '/')

with app.app_context():
    db.create_all()



