
from app import db
from app import login_manager
from flask_login import UserMixin
from sqlalchemy.sql import func

# User model
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(100), unique = True, nullable  = False)
    password = db.Column(db.String(100), nullable = False)
    reviews = db.relationship('Review', backref='user', lazy=True)

# Movie model
class Movie(db.Model):
    __tablename__ = 'movie'
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(255), nullable  = False)
    cover = db.Column(db.String(255))
    year = db.Column(db.String(100))
    like = db.Column(db.Integer, default=0)
    review = db.relationship('Review', backref='movie', lazy=True)

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

# Review model
class Review(db.Model):
    __tablename__ = 'review'
    id = db.Column(db.Integer, primary_key = True)
    content = db.Column(db.String(10000))
    date = db.Column(db.DateTime(timezone = True), default = func.now())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    movie_id = db.Column(db.Integer, db.ForeignKey('movie.id'))

