
// function: delete a review
document.addEventListener('DOMContentLoaded', function() {
    const deleteButton = document.querySelectorAll('.delete-button');

    deleteButton.forEach(button => {
        button.addEventListener('click', function() {
            const reviewID = this.getAttribute('data-review-id');
            
            fetch('/delete-review/' + reviewID, { method: 'POST'})
            const listItem = this.parentElement;
            listItem.remove();

        })
    })
});