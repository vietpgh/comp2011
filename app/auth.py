
from flask import Blueprint, render_template, flash, redirect, url_for, request
from app import app, db
from .models import User
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, login_required, logout_user, current_user

auth_bp = Blueprint('auth', __name__)

# Sign up for a new account
@auth_bp.route('/sign-up', methods = ['GET', 'POST'])
def sign_up():
    if request.method == 'POST':
        username = request.form.get('username')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')

        user = User.query.filter_by(username = username).first()

        # validates user inputs
        if user:
            flash('Username already exists. Please choose a different username.', category = 'error')

        elif len(username) < 7:
            flash('Username must be greater than 6 characters.', category = 'error')

        elif len(password1) < 8:
            flash('Password must be at least 8 characters.', category = 'error')

        elif password1 != password2:
            flash('Passwords do not match. Please check again.', category = 'error')

        else:
            new_user = User(username = username, 
                            password = generate_password_hash(password1, method = 'sha256'))

            db.session.add(new_user)
            db.session.commit()

            flash('Account created successfully! Congrats!', category = 'success')

            return redirect(url_for('auth.login'))
    
    return render_template('sign_up.html', user = current_user)
                          
# Login to user account                     
@auth_bp.route('/login', methods = ['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')

        user = User.query.filter_by(username = username).first()

        if user:
            if check_password_hash(user.password, password):
                login_user(user, remember = True)

                return redirect(url_for('views.home'))

            else:
                flash('Incorrect password! Please try again.', category = 'error')
                
        else:
            flash('Username does not exist. Please try again or sign up.', category = 'error')

    return render_template('login.html', user = current_user)

# Log out of user account
@auth_bp.route('/logout')
@login_required

def logout():
   logout_user()

   return redirect(url_for('auth.login'))                 



