
from flask import Blueprint, render_template, flash, redirect, url_for, request
from flask_login import login_required, current_user
from app import app, db
from .models import Movie, Review

views_bp = Blueprint('views', __name__)

# homepage
@views_bp.route('/')
@login_required
def home():
   movies = Movie.query.all()

   return render_template('index.html', movies = movies)

# add movies to database                                          
@views_bp.route('/add-movie')
def add_movie():
   movie_data = [
      {'title': 'Tales From Earthsea', 'cover': '../static/images/m1.jpg', 'year': '2006'},
      {'title': 'Ocean Waves', 'cover': '../static/images/m2.jpg', 'year': '1993'},
      {'title': 'Ghiblies: Episode 2', 'cover': '../static/images/m3.jpg', 'year': '2002'},
      {'title': 'Pom Poko', 'cover': '../static/images/m4.jpg', 'year': '1994'},
      {'title': 'The Red Turtle', 'cover': '../static/images/m5.jpg', 'year': '2016'},
      {'title': 'The Cat Returns', 'cover': '../static/images/m6.jpg', 'year': '2002'},
      {'title': 'My Neighbors The Yamadas', 'cover': '../static/images/m7.jpg', 'year': '1999'},
      {'title': 'From Up On Poppy Hill', 'cover': '../static/images/m8.jpg', 'year': '2011'},
      {'title': 'Whisper Of The Heart', 'cover': '../static/images/m9.jpg', 'year': '1995'},
      {'title': 'Only Yesterday', 'cover': '../static/images/m10.jpg', 'year': '1991'},
      {'title': 'When Marnie Was There', 'cover': '../static/images/m11.jpg', 'year': '2014'},
      {'title': 'The Secret World Of Arrietty', 'cover': '../static/images/m12.jpg', 'year': '2010'},
      {'title': 'The Wind Rises', 'cover': '../static/images/m13.jpg', 'year': '2013'},
      {'title': 'Castle In The Sky', 'cover': '../static/images/m14.jpg', 'year': '1986'},
      {'title': "Howl's Moving Castle", 'cover': '../static/images/m15.jpg', 'year': '2004'},
      {'title': 'Nausicaa Of The Valley Of The Wind', 'cover': '../static/images/m16.jpg', 'year': '1984'},
      {'title': 'The Tale Of The Princess Kaguya', 'cover': '../static/images/m17.jpg', 'year': '2013'},
      {'title': "Kiki's Delivery Service", 'cover': '../static/images/m18.jpg', 'year': '1989'},
      {'title': 'Ponyo', 'cover': '../static/images/m19.jpg', 'year': '2008'},
      {'title': 'Porco Rosso', 'cover': '../static/images/m20.jpg', 'year': '1992'},
      {'title': 'My Neighbor Totoro', 'cover': '../static/images/m21.jpg', 'year': '1988'},
      {'title': 'Princess Mononoke', 'cover': '../static/images/m22.jpg', 'year': '1997'},
      {'title': 'Grave Of The Fireflies', 'cover': '../static/images/m23.jpg', 'year': '1998'},
      {'title': 'Spirited Away', 'cover': '../static/images/m24.jpg', 'year': '1991'},
   ]

   for data in movie_data:
      movie = Movie(title=data['title'], cover=data['cover'], year=data['year'])
      db.session.add(movie)
   
   db.session.commit()

   return redirect(url_for('views.home'))

# delete movies from database                                          
@views_bp.route('/delete-movie')
def delete_movie():
   movies = Movie.query.all()

   for movie in movies:
      db.session.delete(movie)
   
   db.session.commit()

   return redirect(url_for('views.home'))

# like button: uncompleted                     
@views_bp.route('/like', methods=['POST'])
def like():

   return redirect(url_for('views.home'))

# show the favorite movies: uncompleted                      
@views_bp.route('/favorites')
def favorite():

   return render_template('favorites.html'
                          )                         

# show all reviews
@views_bp.route('/reviews')
def reviews():
   reviews = Review.query.all()
   
   return render_template('reviews.html',
                          reviews = reviews)

# add new review
@views_bp.route('/add-review', methods=['GET', 'POST'])
def addReview():
   if request.method == 'POST':
      review = request.form.get('review')
      movie_name = request.form.get('movie-name')

      movie = Movie.query.filter_by(title=movie_name).first()
      if movie:
         new_review = Review(content=review, user_id=current_user.id, movie_id=movie.id)
         db.session.add(new_review)
      else:
         flash('No such movies found. Please try another movie.', category='error')

      db.session.commit()
   
   return render_template('addreview.html')
                          
# delete an existing review
@views_bp.route('/delete-review/<int:review_id>', methods=['POST'])
def deleteReview(review_id):
   review = Review.query.get(review_id)

   db.session.delete(review)
   db.session.commit()

   # Return to reviews list after deletion
   return redirect(url_for('views.reviews'))
